import React from 'react';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { DetailScreen, ChartScreen } from '../screens';
import routes from './routes';
import { Text, TouchableOpacity } from 'react-native';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={routes.Chart} component={ChartScreen} options={() => ({
        headerTitleAlign: 'center',
      })}/>
      <Stack.Screen
        name={routes.Detail}
        component={DetailScreen}
        options={({ navigation }) => ({
          title: 'Your Screen',
          headerTitleAlign: 'center',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Text>Close</Text>
            </TouchableOpacity>
          ),
        })}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
