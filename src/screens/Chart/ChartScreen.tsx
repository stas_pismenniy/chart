import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, TouchableOpacity, Dimensions, FlatList, View } from "react-native";

import styles from './styles';
import { LineChart } from 'react-native-chart-kit';
import { Item, NavigationProps, ItemParams } from "../../global/global";

type ChartScreenProps = NavigationProps<'Detail'>;

const screenWidth = Dimensions.get('window').width - 20;

const formatLabel = (date: Date) => {
  const year = date.getFullYear().toString().slice(2);
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  return `${month}-${day}-${year}`;
};

const generateRandomData = () => {
  const datasets = [{ data: [] }];
  const labels = [];
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  // Generate 4 random months
  for (let i = 0; i < 4; i++) {
    const randomValue = Math.floor(Math.random() * 51) + 50;
    const randomMonth = Math.floor(Math.random() * 12);
    const monthName = monthNames[randomMonth];
    const randomDate = new Date(2021, randomMonth, Math.floor(Math.random() * 28) + 1);
    const label = {label: formatLabel(randomDate), name: monthName};

    // @ts-ignore
    datasets[0].data.push(randomValue);
    labels.push(label);
  }

  return { datasets, labels };
};

const chartConfig = {
  backgroundGradientFrom: '#fff',
  backgroundGradientTo: '#fff',
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  strokeLinecap: 'round',
  propsForBackgroundLines: {
    strokeDasharray: [0]
  }
};

const ChartScreen = ({ navigation }: ChartScreenProps) => {
  const [chartData, setChartData] = useState(generateRandomData());

  useEffect(() => {
    setChartData(generateRandomData());
  }, []);

  const navigateToDetail = (item: Item) => {
    navigation.navigate('Detail', { item });
  };
  const labels = Object.values(chartData.labels).map(item => item.label)
  const chartDataAssets = chartData.datasets

  const flatListData = chartData.labels.map((label, index) => ({
    label: label.label,
    month: label.name,
    value: chartDataAssets[0].data[index],
  }));

  const renderItem = ({item, index}: ItemParams) => {
    const isLastItem = index === flatListData.length - 1
    return (
      <TouchableOpacity onPress={() => {
        navigateToDetail(item)
      }} style={[isLastItem ? [styles.item, styles.noBorder]: styles.item]}>
        <View>
          <Text style={styles.month}>{item.month}</Text>
          <Text>{item.label}</Text>
        </View>
        <Text>{item.value}</Text>
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.elevation}>
      <LineChart
        style={styles.chart}
        data={{ datasets: chartDataAssets, labels }}
        width={screenWidth}
        height={220}
        chartConfig={chartConfig}
        withShadow={false}
        yAxisInterval={12}
      />
      <FlatList
        data={flatListData}
        style={styles.flatList}
        contentContainerStyle={styles.flatListContainerStyle}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderItem}
      />
      </View>
    </SafeAreaView>
  );
};

export default ChartScreen;
