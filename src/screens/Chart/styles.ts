import { Platform, StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3'
  },
  month: {marginBottom: 5, fontWeight: 'bold'},
  noBorder: {
    borderBottomWidth: 0
  },
  flatList: {
    backgroundColor: '#fff',
    borderRadius: 5
  },
  flatListContainerStyle: {
    justifyContent: 'center',
    alignContent: 'center'
  },
  elevation: {
    margin: 10,
    elevation: 3, // Add elevation for Android box shadow
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
      },
    })
  },
  chart: {marginBottom: 10, borderRadius: 5}
});
