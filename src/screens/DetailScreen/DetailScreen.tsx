import React, { useCallback, useState } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';

import WebView from 'react-native-webview';
import Modal from 'react-native-modal';

import styles from './styles';
import { DetailScreenProps } from '../../global/global';

const DetailScreen = ({route}: DetailScreenProps) => {
  const {item} = route.params

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const generateTitleValueArray = useCallback(() => {
    const dataArray = [];

    dataArray.push({ title: 'Month', value: item.month });
    dataArray.push({ title: 'Date', value: item.label });
    dataArray.push({ title: 'Height', value: Math.floor(Math.random() * 100) });
    dataArray.push({ title: 'Weight', value: Math.floor(Math.random() * 100) });
    dataArray.push({ title: 'Body Fat', value: Math.floor(Math.random() * 100) });
    dataArray.push({ title: 'Fat Weight', value: Math.floor(Math.random() * 100) });
    dataArray.push({ title: 'Lean Body Mass', value: Math.floor(Math.random() * 100) });
    dataArray.push({ title: 'Body Mass Index', value: Math.floor(Math.random() * 100) });
    dataArray.push({ title: 'Fitness Score', value: item.value });
    dataArray.push({ title: 'Wellness Score', value: Math.floor(Math.random() * 100) });

    return dataArray;
  }, []);

  const webPageUrl = 'https://google.com';

  const flatListData = generateTitleValueArray()

  const flatListFooter = () => {
    return (
      <View style={styles.measurementContainer}>
        <View style={styles.measurementWrap}>
          <View style={styles.measurementTitleWrap}>
            <Text style={styles.measurementTitle}>Measurement Information</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.measurementTitleWrap} onPress={toggleModal}>
          <Text style={styles.measurementTitleAction}>Understanding Your Measurements?</Text>
        </TouchableOpacity>

      </View>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.elevation}>
        <FlatList
          ListFooterComponent={flatListFooter}
          data={flatListData}
          style={styles.flatList}
          contentContainerStyle={styles.flatListContainerStyle}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            const isLastItem = index === flatListData.length - 1;
            return (
              <View style={[isLastItem ? [styles.item, styles.noBorder] : styles.item]}>
                <Text>{item.title}</Text>
                <Text>{item.value}</Text>
              </View>
            );
          }}
        />

      </View>

      <Modal
        isVisible={isModalVisible}
        style={styles.modal}
        animationIn="slideInUp"
        animationInTiming={300}
        animationOut="slideOutDown"
        animationOutTiming={300}
        onBackdropPress={toggleModal}
        avoidKeyboard={true}>

        <SafeAreaView style={styles.safeArea}>
          <View style={styles.modalContent}>
            <WebView source={{ uri: webPageUrl }} />
            <TouchableOpacity onPress={toggleModal} style={styles.closeButton}>
              <Text>Close</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>

      </Modal>
    </SafeAreaView>
  );
};

export default DetailScreen;
