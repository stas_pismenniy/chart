import { Platform, StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  wrapper: {
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    padding: 15,
    borderBottomWidth: 2,
    borderBottomColor: '#D3D3D3'
  },
  flatList: {
    // backgroundColor: '#fff',
    borderRadius: 5
  },
  flatListContainerStyle: {
    justifyContent: 'center',
    alignContent: 'center'
  },
  elevation: {
    elevation: 3, // Add elevation for Android box shadow
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
      },
    })
  },
  noBorder: {
    borderBottomWidth: 0
  },
  modal: {
    margin: 20,
  },
  modalContent: {
    flex: 1,
  },
  closeButton: {
    position: 'absolute',
    top: 20,
    right: 20,
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 3, // Add elevation for Android box shadow
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
      },
    })
  },
  safeArea: {
    flex: 1,
  },
  measurementContainer: { backgroundColor: "#fff", marginTop: 10 },
  measurementWrap: {
    borderBottomWidth: 1, borderBottomColor: "#D3D3D3"
  },
  measurementTitleWrap: { padding: 15 },
  measurementTitle: { fontWeight: 'bold' },
  measurementTitleAction: { color: 'gray' }
});
