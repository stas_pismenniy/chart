import { RouteProp } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

/** @description CalendarProps */
type Item = {
  label: string,
  month: string,
  value: number,
}

type ItemParams = {
  item: Item,
  index: number
}

type RootStackParamList = {
  Chart: undefined;
  Detail: {item: Item}};

type NavigationProps<T extends keyof RootStackParamList> = {
  navigation: NativeStackNavigationProp<RootStackParamList, T>;
};

type DetailScreenRouteProp = RouteProp<RootStackParamList, 'Detail'>;


type DetailScreenProps = {
  route: DetailScreenRouteProp;
};
